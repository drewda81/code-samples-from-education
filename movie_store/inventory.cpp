// inventory.cpp
// -----------------------------------------------------------------------------
// command to display the entire inventory for this store
// -----------------------------------------------------------------------------

#include "inventory.h"

// default constructor
// -----------------------------------------------------------------------------
Inventory::Inventory() {}

// destructor
// -----------------------------------------------------------------------------
Inventory::~Inventory() {}

// execute
// -----------------------------------------------------------------------------
// An unfortunately-named class (but necessarily so to match the vernacular used
// in the assignment description), Inventory outputs to the console all movies
// in this Store's Stock.
// -----------------------------------------------------------------------------
void Inventory::execute( Stock& theStock, Customers& theCustomers ) {
  cout << endl << "Inventory" << endl;
  theStock.displayAll();
}
