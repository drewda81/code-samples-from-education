// -------------------- stock.h -----------------------

/*
 *  Stock class keeps our inventory's information.
 *
 */

#ifndef STOCK_H_
#define STOCK_H_

#include "movie.h"
#include "bintree.h"
#include <sstream>

class Stock
{
public:
  Stock();
  ~Stock();

  // actions
  void addStock( Movie& );      // Adds stock to movies

  void displayAll();                  // Displays entire inventory 
  Movie* increment( Movie& );    // decrement stock for particular movie
		                                  // returns true if successful;
		                                  // false otherwise
  Movie* decrement( Movie& );    // increment stock for particular movie 
                                      // returns true if successful;
                                      // false otherwise
private:

  BinTree movies;

//  set<ComedyMovie> comedyMovies;
//  set<DramaMovie> dramaMovies;
//  set<ClassicMovie> classicMovies;
};
#endif /* STOCK_H_ */
