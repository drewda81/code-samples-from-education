//---------------------- borrow.h -------------------------
/*
* Borrow is a subclass of DocumentableCommand. If stock for
* specified movie is < 1, print error message and return. 
* Else reduce the inventory for a particular movie by one
* and add a Borrow to Customer’s History.
* customer’s history.
*/

#ifndef BORROW_H
#define BORROW_H

#include <string>
#include "documentablecommand.h"
#include "stock.h"
#include "customers.h"

class Borrow : public DocumentableCommand
{
public:
  Borrow( int, Movie& );                // params: cust ID, movie description
  ~Borrow( ); 
  void execute( Stock&, Customers& );

};
#endif // BORROW_H
