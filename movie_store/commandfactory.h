// ------------------ commandfactory.h ---------------

/*
 *  CommandFactory is a static class that extracts a
 *  Command from an input stream and converts it
 *  into a Command and return that Command.
 */
#ifndef COMMANDFACTORY_H_
#define COMMANDFACTORY_H_

#include "command.h"
#include "documentablecommand.h"
#include "inventory.h"
#include "history.h"
#include "borrow.h"
#include "return.h"
#include "movie.h"
#include "classicmovie.h"
#include <sstream>

class CommandFactory
{
public:

    // Constructor, destructor
    CommandFactory();
    ~CommandFactory();

    // function creates a Command object
    Command* makeCommand( istringstream& );

};

#endif /* COMMANDFACTORY_H_ */
