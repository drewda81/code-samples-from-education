// moviefactory.cpp
// -----------------------------------------------------------------------------

#include "moviefactory.h"

MovieFactory::MovieFactory(){}

MovieFactory::~MovieFactory(){}

Movie* MovieFactory::makeMovie( istringstream &input )
{
  // get category
  char category;
  category = input.get();
  input.get();
  input.get();

  int currentDigit;
  int stock = 0;
  while ( ( currentDigit = input.get() ) != ',' )
    stock = stock * 10 + currentDigit - 48; // convert for ASCII

  input.get(); // burn a char
  string director;
  char currentChar;
  while ( ( currentChar = input.get() ) != ',')
    director += currentChar;

  input.get(); // burn a char
  string title;
  while ( ( currentChar = input.get() ) != ',')
    title += currentChar;

  // process based on category
  switch (category) {
    case 'F':
    {
      int year;
      input >> year;
      return new Movie( category, 'D', stock, director, title, year, "", 1 );
    }
    case 'D':
    {
      int year;
      input >> year;
      return new Movie( category, 'D', stock, director, title, year, "", 1 );
    }
    case 'C':
    {
      string firstName, lastName;
      int month, year;
      input >> firstName >> lastName >> month >> year;
      string actor = firstName + " " + lastName;
      return new ClassicMovie( category, 'D', stock, director, title, year, 
                              actor, month);
    }
    default:
    {
      cout << "unrecognized category:" << category << endl;
      return NULL;
    }
  }
}
