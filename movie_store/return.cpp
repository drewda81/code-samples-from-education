#include "return.h"

// default constructor
// -----------------------------------------------------------------------------
Return::Return( int aCustomerID, Movie& aDummyMovie )
              : DocumentableCommand( aCustomerID, aDummyMovie ) 
                                         
{ }

// destructor
// -----------------------------------------------------------------------------
Return::~Return( )
{ 
  delete dummyMovie;
}

// execute
// -----------------------------------------------------------------------------
// Executes this Command on the Store's Stock and Customers, effectively
// reducing the Stock for a specified Movie by one and adding a transaction
// to the Specified Customer's History.
// -----------------------------------------------------------------------------
void Return::execute( Stock& theStock, Customers& theCustomers )
{
  Customer* theCustomer = theCustomers.get(customerID);
  if ( !theCustomer )
  {
    cout << "customer not found" << endl;
    return;
  }

  Movie* theMovie = theStock.increment(*dummyMovie);
  if ( theMovie )
  {
    theCustomer->document('R', *theMovie);
  }
  else
    cout << "movie not found" << endl;

}
