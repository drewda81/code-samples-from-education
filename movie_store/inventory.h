// --------------- inventory.h ----------------

/*
 * Inventory class is a subclass of the 
 * Command class. The inventory command  
 * displays all of our inventory.
 *
 */

#ifndef INVENTORY_H_
#define INVENTORY_H_

#include "command.h"
#include "stock.h"
#include "customers.h"

class Inventory:public Command
{
public:

  // Constructor and destructor
  Inventory();
  ~Inventory();
  void execute( Stock&, Customers& );
};
#endif /* INVENTORY_H_ */

