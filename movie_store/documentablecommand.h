//----------------- documentablecommand.h --------------

/*
*  DocumentableCommand
*  A type of command that is added to the customer’s  
*  history when executed
*/

#ifndef DOCUMENTABLECOMMAND_H
#define DOCUMENTABLECOMMAND_H

#include "movie.h"
#include "command.h"
#include <string>
#include <iostream>

class DocumentableCommand : public Command
{

public:
  DocumentableCommand( int, Movie& );
  virtual ~DocumentableCommand( );

protected:
  int customerID;
  Movie* dummyMovie;
};

#endif // DOCUMENTABLECOMMAND_H
