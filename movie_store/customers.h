// ------------------- customers.h -----------------
/*
 *  A hash table for storing Customer information 
 */

#ifndef CUSTOMERS_H_
#define CUSTOMERS_H_

#include "customer.h"

class Customers
{

public:
  Customers();
  ~Customers();
  Customer* get( int );         // returns Customer with specified ID
  bool add( Customer& );        // hashes and stores a Customer 
  void displayAll();

private:
  Customer* table[29];
};

#endif // ndef CUSTOMERS
