// classicmovie.cpp
// -----------------------------------------------------------------------------
// Subclass of Movie. Includes information for month of release and major actor
// or actress. Uses
// ----------------------------------------------------------------------------

#include "classicmovie.h"

// constructor
// -----------------------------------------------------------------------------
ClassicMovie::ClassicMovie( char aCategory, char aFormat, int someStock, 
                            string aDirector, string aTitle, int aYear, 
                            string anActor, int aMonth) : Movie( aCategory, 
                            aFormat, someStock, aDirector, aTitle, aYear, 
                            anActor, aMonth)
{ }


// destructor
// -----------------------------------------------------------------------------
ClassicMovie::~ClassicMovie( )
{ }

// operator <
// -----------------------------------------------------------------------------
// Compares year, then month, then actor. 
// -----------------------------------------------------------------------------
//bool ClassicMovie::operator<( const Movie & rhs) const
//{
//  return true;
//}
//
//
//bool ClassicMovie::operator>( const Movie & rhs) const
//{
//  return false;
//}
//
//bool ClassicMovie::operator==( const Movie & rhs) const
//{
//  return false;
//}
//
//bool ClassicMovie::operator!=( const Movie& rhs) const
//{
//  return true;
//}

//// operator <
//// -----------------------------------------------------------------------------
//// Compares year, then month, then actor. 
//// -----------------------------------------------------------------------------
//bool ClassicMovie::operator<( const ClassicMovie & rhs) const
//{
//  if ( this->year < rhs.year )
//    return true;
//  else if ( this->year == rhs.year )
//    if ( this->month < rhs.month )
//      return true;
//    else if ( this->month == rhs.month )
//      if ( this->actor[0] < rhs.actor[0] )
//        return true;
//      else return false;
//    else return false;
//  else return false;
//}
//
//
//// operator >
//// -----------------------------------------------------------------------------
//// Compares year, then month, then actor. 
//// -----------------------------------------------------------------------------
//bool ClassicMovie::operator>( const ClassicMovie & rhs) const
//{
//  if ( this->year > rhs.year )
//    return true;
//  else if ( this->year == rhs.year )
//    if ( this->month > rhs.month )
//      return true;
//    else if ( this->month == rhs.month )
//      if ( this->actor[0] > rhs.actor[0] )
//        return true;
//      else return false;
//    else return false;
//  else return false;
//}
//
//
//// operator ==
//// -----------------------------------------------------------------------------
//// Compares year, then month, then actor. 
//// -----------------------------------------------------------------------------
//bool ClassicMovie::operator==( const ClassicMovie & rhs) const
//{
//  // does not consider actor
//  if ( this->format == rhs.format &&
//       this->director == rhs.director &&
//       this->title == rhs.title &&
//       this->year == rhs.year &&
//       this->month == rhs.month )
//    return true;
//  else return false;
//}
//
//// operator !=
//// -----------------------------------------------------------------------------
//// Compares year, then month, then actor. 
//// -----------------------------------------------------------------------------
//bool ClassicMovie::operator!=( const ClassicMovie & rhs) const
//{
//  return !( *this == rhs );
//}

// display
// -----------------------------------------------------------------------------
// Calls display from Movie and adds month and actor
// -----------------------------------------------------------------------------
void ClassicMovie::display( ) const
{
  Movie::display( );
  cout << ", " << actor << " " << month;
}


// toString 
// -----------------------------------------------------------------------------
// Calls toString from Movie and adds month and actor
// -----------------------------------------------------------------------------
string ClassicMovie::toString( )
{
  stringstream ss;
  ss << month;
  string str = ss.str();
  return ( Movie::toString( ) + ", " + actor + " " + str );
}
