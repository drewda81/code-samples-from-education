// classicmovie.h
// ----------------------------------------------------------------------------
// Subclass of Movie. Includes information for month of release and major actor
// or actress.
// ----------------------------------------------------------------------------

#ifndef CLASSICMOVIE_H
#define CLASSICMOVIE_H

#include "movie.h"
#include <sstream>
class ClassicMovie:public Movie
{

public:
  // constructor (params: category, format, stock, director, title, year, 
  // actor, month)
  ClassicMovie( char, char, int, string, string, int, string, int);

  // destructor
  ~ClassicMovie();
  
  string toString();     // return a string representation
  void display( ) const; // override the display method from Movie
  
};

#endif /* CLASSICMOVIE_H */
