// ------------------- history.h --------------------

/*
 * History class is a subclass of the Command class.
 * The History command outputs the action history
 * for a specified customer.
 */
 
#ifndef HISTORY_H
#define HISTORY_H

#include "command.h"
#include "stock.h"
#include "customers.h"

class History : public Command
{
public:

  History( int ); // param: customerID
  ~History( ) { };

  void execute( Stock&, Customers& );

private:
  int customerID;

};
#endif // HISTORY_H
