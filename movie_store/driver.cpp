#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "business.h"
#include "moviefactory.h"
#include "commandfactory.h"
using namespace std;

int main(int argc, char** argv) {
  if (argc != 4)
  {
    cout << "Please enter three file names: customers, movies, commands" << endl;
    return 1;
  }

  Business theBusiness;

  // process customers
  ifstream customerData( argv[1] );
  int id;
  string firstName, lastName;
  while ( customerData >> id >> firstName >> lastName )
  {
    string name = firstName + " " + lastName;
    Customer* cust = new Customer( id, name );
    theBusiness.addCustomer( *cust );
  }
  customerData.close();
  cout << "Finished processing customers" << endl;

  // process movies
  MovieFactory mf;
  ifstream movieData( argv[2] );
  string line;
  while ( getline( movieData, line ) )
  {
    istringstream iss( line );
    Movie* aMovie = mf.makeMovie( iss );
    if ( aMovie )
      theBusiness.addMovie( *aMovie );
  }
  movieData.close();
  cout << "Finished processing movies" << endl;

  // process commands
  CommandFactory cf;
  ifstream commandData( argv[3] );
  while ( getline( commandData, line ) )
  {
    istringstream iss( line );
    Command* aCommand = cf.makeCommand( iss );
    if ( aCommand )
      theBusiness.execute( *aCommand );
  }
  commandData.close();
  cout << "Finished processing commands" << endl;

  return 0;
}
