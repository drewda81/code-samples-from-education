// ------------------ business.h ---------------
/*
 *  Business manages business logic and store business
 *  data. Driver executes commands on Business.
 */

#ifndef BUSINESS_H_
#define BUSINESS_H_

#include "stock.h"
#include "customers.h"
#include "movie.h"
#include "customer.h"
#include "command.h"

using namespace std;

class Business {
public:
  Business( );
  ~Business( );
  void execute( Command& );
  void addMovie( Movie& );
  void addCustomer( Customer& );
  void displayCustomers( );

private:
  Stock theStock;                  // this store’s current stock
  Customers theCustomers;          // this store’s customer information
};

#endif // ndef BUSINESS_H_

