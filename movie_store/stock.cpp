#include "stock.h"

// default constructor
// -----------------------------------------------------------------------------
Stock::Stock() {};

// destructor
// -----------------------------------------------------------------------------
Stock::~Stock() {};

// displayAll
// -----------------------------------------------------------------------------
// Outputs to the console details and count for each Movie in this Store's Stock
// -----------------------------------------------------------------------------
void Stock::displayAll()
{
  cout << movies << endl;
}

// addStock
// -----------------------------------------------------------------------------
// Add some quantity of a movie to existing stock.
// -----------------------------------------------------------------------------
void Stock::addStock( Movie& aMovie )
{
  Movie* result;

  bool success = movies.retrieve( aMovie, result );
  if (success)
    result->add( aMovie.getStock() );
  else
    movies.insert( aMovie );
}


// increment
// -----------------------------------------------------------------------------
// Increments the Stock for a specified Movie by 1.
// -----------------------------------------------------------------------------
Movie* Stock::increment( Movie& dummyMovie )
{
  Movie* result;
  bool success = movies.retrieve( dummyMovie, result );
  if (success)
  {
    result->add(1);
    return result;
  }
  else return NULL; 
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
Movie* Stock::decrement( Movie& dummyMovie )
{
  Movie* result;
  bool success = movies.retrieve( dummyMovie, result );
  if (success)
  {
    if (result->remove(1))
    return result;
  }
  else return NULL; 
}
