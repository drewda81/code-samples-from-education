// movie.h
// ----------------------------------------------------------------------------
// Defines a Movie object in this system.
// ----------------------------------------------------------------------------

#ifndef MOVIE_H
#define MOVIE_H

#include <string>
#include <iostream>
#include <sstream>
using namespace std;

class Movie
{
public:
  // constructor (params: category, format, stock, director, title, year, 
  // actor, month)
  Movie(char, char, int, string, string, int, string, int);
  Movie(const Movie& );

  ~Movie();

  // comparisons
  virtual bool operator!=( const Movie& rhs ) const;
  virtual bool operator==( const Movie& rhs ) const;
  virtual bool operator<( const Movie& rhs ) const;
  virtual bool operator>( const Movie& rhs ) const;

  // getters
  char getCategory( ) const { return category; }
  char getFormat( ) const { return format; }
  string getDirector( ) const { return director; }
  string getTitle( ) const { return title; }
  int getYear( ) const { return year; }
  int getStock( ) const { return stock; }
  string getActor( ) const { return actor; }
  int getMonth( ) const { return month; }
  
  // actions
  virtual void display( ) const;
  void add( int );
  bool remove( int );
  virtual string toString( );
  
protected:

  char category;
  char format;
  string director;
  string title;
  int year;
  int stock;
  string actor;
  int month;
  
};
#endif /* MOVIE_H */

