// ------------------- moviefactory.h -------------------
/*
 *  A static class that extracts data from an  
 *  input stream and convert it into the appropriate
 *   Movie, then returns that Movie.
 */

#ifndef MOVIEFACTORY_H_
#define MOVIEFACTORY_H_

#include "movie.h"
#include <sstream>
#include "classicmovie.h"

class MovieFactory
{
public:

    // Constructor
    MovieFactory();
    ~MovieFactory();

    // Creates a Movie object
    Movie* makeMovie( istringstream& );
        // Movie data file will be read a line at a time 
        // and assumed to be correctly formatted. Movies  
        // with invalid labels will be discarded, all 
        // others will be returned. 
};

#endif /* MOVIEFACTORY_H_ */

