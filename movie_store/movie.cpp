// movie.cpp
// -----------------------------------------------------------------------------
// Representation of a Movie in for this Store. Appears in both Stock and a
// Customer's History.
// -----------------------------------------------------------------------------

#include "movie.h"

// default constructor
// -----------------------------------------------------------------------------
Movie::Movie( char aCategory, char aFormat, int someStock, string aDirector, 
              string aTitle, int aYear, string anActor, int aMonth) 
              : category( aCategory ), format( aFormat ), stock( someStock ),
              director( aDirector ), title( aTitle ), year( aYear ),
              actor(anActor), month( aMonth )
{
}

// destructor
// -----------------------------------------------------------------------------
Movie::~Movie( )
{ }

// display
// -----------------------------------------------------------------------------
// Output to console the details of this Movie. Does not end the line.
// -----------------------------------------------------------------------------
void Movie::display( ) const
{
  cout << category << ", " << format << ", " << stock << ", " << director 
       << ", " << title << ", " << year;
}

// toString
// -----------------------------------------------------------------------------
// return a string representation of this movie
// -----------------------------------------------------------------------------
string Movie::toString( ) 
{
  string result;
  result.push_back(category);
  result += ", ";
  result.push_back(format);
  result += ", ";
  result += director;
  result += ", ";
  result += title;
  result += ", ";
  stringstream ss;
  ss << year;
  string str = ss.str();
  result += str;
  return result;

}

// operator==
// -----------------------------------------------------------------------------
bool Movie::operator==( const Movie& rhs ) const
{
  if ( this->category == rhs.category )
  {
    if ( category == 'C' )
    {
      if ( this->year == rhs.year && this->actor == rhs.actor )
        return true;
      else return false;
    }
    else if ( this->category == 'D' )
    {
      if ( this->director == rhs.director && this->title == rhs.title )
        if ( this->title == rhs.title )
          return true;
        else return false; else return false;
    }
    else // if ( this->category == 'F' )
    {
      if ( this->title == rhs.title && this->year == rhs.year)
        return true;
      else return false;
    }
  }
  else return false;
}


// operator!=
// -----------------------------------------------------------------------------
bool Movie::operator!=( const Movie& rhs ) const
{
  return !(*this == rhs );
}

// operator>
// -----------------------------------------------------------------------------
bool Movie::operator>( const Movie& rhs ) const
{
  if ( this->category < rhs.category ) // C > D > F
  {
    return true;
  }
  else if ( this->category == rhs.category )
  {
    if ( this->category == 'C' )
    {
      if ( this->year > rhs.year )
        return true;
      else if ( this->year == rhs.year )
        if ( this->actor > rhs.actor )
          return true;
        else return false;
      else return false;
    }
    else if ( this->category == 'D' )
    {
      if ( this->director > rhs.director )
        return true;
      else if ( this->director == rhs.director )
        if ( this->title > rhs.title )
          return true;
        else return false;
      else return false;
    }
    else // if ( this->category == 'F' )
    {
      if ( this->title > rhs.title )
        return true;
      else if ( this->title < rhs.title )
        if ( this->year > rhs.year )
          return true;
        else return false;
      else return false;
    }
  }
  else return false;
}

// operator<
// -----------------------------------------------------------------------------
bool Movie::operator<( const Movie& rhs ) const
{
  return !( *this > rhs || *this == rhs );
}

// add
// -----------------------------------------------------------------------------
// Adds stock to this movie
// -----------------------------------------------------------------------------
void Movie::add( int someStock )
{
  stock += someStock;
}

// removeStock
// -----------------------------------------------------------------------------
// Removes stock from this movie
// -----------------------------------------------------------------------------
bool Movie::remove( int someStock )
{
  if ( stock > 0)
  {
    stock -= someStock;
    return true;
  }
  else
  {
    cout << "no copies remaining" << endl;
    return false;
  }
}
