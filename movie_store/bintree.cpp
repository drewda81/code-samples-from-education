// -------------------------------- BinTree.cpp --------------------------------
// Andrew Andersen CSS343A
// Date: 2016-01-30
// Last Modified: 2016-01-30
// -----------------------------------------------------------------------------
// Implementation of a Binary Tree
// -----------------------------------------------------------------------------
// Notes:
// *Max size of array specified in MAX_ARRAY_SIZE global constant 
// -----------------------------------------------------------------------------

#include "bintree.h"

// --------------------------- Default Construtor ------------------------------ 
// Description: Instantiates an object of type BinTree.
// -----------------------------------------------------------------------------
BinTree::BinTree( )
{
  root = NULL;
}

// ------------------------------- Destructor ---------------------------------- 
// Description: destroys this BinTree 
// -----------------------------------------------------------------------------
BinTree::~BinTree( )
{
  destroyTree( root );
}

// ------------------------------- makeEmpty ----------------------------------- 
// Description: removes all nodes from this BinTree
// -----------------------------------------------------------------------------
void BinTree::makeEmpty( )
{
  if ( root != NULL )
  {
    destroyTree( root );
  }
  root = NULL;
}


// ------------------------------- destroyTree --------------------------------- 
// Description: helper function for makeEmpty and Destructor 
// -----------------------------------------------------------------------------
void BinTree::destroyTree( Node* subTreePtr )
{
  if ( subTreePtr != NULL )
  {
    destroyTree( subTreePtr->left );
    destroyTree( subTreePtr->right );
    delete subTreePtr->data;
    delete subTreePtr;
  }
}

// ------------------------------- << Operator --------------------------------- 
// Description: Outputs this BinTree to an ostream 
// -----------------------------------------------------------------------------
ostream& operator<<( ostream& output, const BinTree& input )
{
  input.printHelper( input.root, output );
  cout << endl;
  return output;
}

// ------------------------------- printHelper --------------------------------- 
// Description: helper function for operator<< function
// -----------------------------------------------------------------------------
void BinTree::printHelper( Node* current, ostream& output ) const
{
  // traverse in order and append current's data to output
  if ( current != NULL )
  {
    printHelper( current->left, output );
    current->data->display( ); 
    cout << endl;
    printHelper( current->right, output );
  }
}

// --------------------------------- insert ------------------------------------ 
// Description: inserts newData into the tree; returns false if newData already
// exists in the tree.
// -----------------------------------------------------------------------------
bool BinTree::insert( Movie& newData )
{
  // create an initialize members of new node to insert
  Node* newNodePtr = new Node;
  newNodePtr->data = &newData;
  newNodePtr->left = NULL;
  newNodePtr->right = NULL;

  // call recursive helper
  root = insertHelper( root, newNodePtr );

  return true;
}

// ------------------------------ insertHelper --------------------------------- 
// Description: recursive helper method for insert
// -----------------------------------------------------------------------------
BinTree::Node* BinTree::insertHelper( Node* subTreePtr, Node* newNodePtr )
{
  if (subTreePtr == NULL )
  {
    return newNodePtr;
  }
  else if ( *subTreePtr->data == *newNodePtr->data )
  {
    // delete duplicate entry
    delete newNodePtr->data;
    delete newNodePtr;
  } 
  else if ( *subTreePtr->data > *newNodePtr->data )
  {
    Node* tempPtr = insertHelper( subTreePtr->left, newNodePtr );
    subTreePtr->left = tempPtr;
  }
  else
  {
    Node* tempPtr = insertHelper( subTreePtr->right, newNodePtr );
    subTreePtr->right = tempPtr;
  }
  return subTreePtr;
}

// --------------------------------- isEmpty ----------------------------------- 
// Description: returns true if this BinTree is empty, false otherwise
// -----------------------------------------------------------------------------
bool BinTree::isEmpty( ) const
{
  return root == NULL;
}

// -------------------------------- retrieve ----------------------------------- 
// Description: searches this BinTree for target and points result at the
// equivilent NodeData in this tree; returns true if target is found, false
// otherwise 
// -----------------------------------------------------------------------------
bool BinTree::retrieve( const Movie& target, Movie*& result )
{
  retrieveHelper( this->root, target, result ); 
}

// -------------------------------- retrieve ----------------------------------- 
// Description: searches this BinTree for target and points result at the
// equivilent Node in this tree; returns true if target is found, false
// otherwise 
// -----------------------------------------------------------------------------
bool BinTree::retrieve( const Movie& target, Node*& result )
{
  retrieveHelper( this->root, target, result ); 
}

// ----------------------------- retrieveHelper ------------------------------- 
// Description: a helper function for the retrieve fucntion that
// points to a NodeData object for the result 
// -----------------------------------------------------------------------------
bool BinTree::retrieveHelper( Node* currentRoot, const Movie& target, Movie*& result )
{
  if ( currentRoot == NULL )
    return false;

  else if ( *currentRoot->data == target )
  {
    result = currentRoot->data;
    return true;
  }

  else if ( *currentRoot->data > target )
    return retrieveHelper( currentRoot->left, target, result );

  else // if ( root->data > target )
    return retrieveHelper( currentRoot->right, target, result );
}

// ----------------------------- retrieveHelper ------------------------------- 
// Description: a helper function for the retrieve fucntion that
// points to a Node object for the result; per requirements, does not assume
// this BinTree is a BST and is thus less efficient than the other helper
// -----------------------------------------------------------------------------
bool BinTree::retrieveHelper( Node* currentRoot, const Movie& target, Node*& result )
{
  if ( currentRoot == NULL )
    return false;

  else if ( *currentRoot->data == target )
  {
    result = currentRoot;
    return true;
  }

    return retrieveHelper( currentRoot->left, target, result );
    return retrieveHelper( currentRoot->right, target, result );
}
