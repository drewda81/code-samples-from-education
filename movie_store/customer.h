// ------------------- customer.h -----------------
/*
 *  Class used to represent a Customer in the
 *  system.
 */

#ifndef CUSTOMER_H_
#define CUSTOMER_H_

#include <iostream>
#include <vector>
#include <string>
#include "movie.h"
using namespace std;

class Customer
{
public:
  Customer( int, string );
  ~Customer( );
  void displayHistory( );
  int getID( ) { return customerID; };
  string getName( ) { return name; };
  void document( char, Movie& );
  void displayAll( );

private:
  int customerID;
  string name;
  vector<string> history;
};

#endif // CUSTOMER_H_
