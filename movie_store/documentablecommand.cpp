// documentablecommand.cpp
// -----------------------------------------------------------------------------
// subclass of command for commands that must be documented in a customer's
// history after being executed
// -----------------------------------------------------------------------------
#include "documentablecommand.h"

DocumentableCommand::DocumentableCommand( int aCustomerID, Movie& aDummyMovie )
                                        : customerID( aCustomerID ), 
                                          dummyMovie( &aDummyMovie )
{}

DocumentableCommand::~DocumentableCommand( )
{}
