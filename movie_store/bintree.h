// -------------------------------- BinTree.cpp --------------------------------
// Andrew Andersen CSS343A
// Date: 2016-01-30
// Last Modified: 2016-01-30
// -----------------------------------------------------------------------------
// Implementation of a Binary Tree
// -----------------------------------------------------------------------------
// Notes:
// *Max size of array specified in MAX_ARRAY_SIZE global constant 
// -----------------------------------------------------------------------------

#ifndef BINTREE_H
#define BINTREE_H
#include"movie.h"

const int MAX_ARRAY_SIZE = 100;

class BinTree {

friend ostream& operator<<( ostream&, const BinTree& );

public:

	BinTree();								// constructor
	~BinTree();								// destructor, calls makeEmpty	

	bool isEmpty() const;					// true if tree is empty, otherwise false
	void makeEmpty();						// make the tree empty so isEmpty returns true
	bool insert(Movie&);
	bool retrieve( const Movie& target, Movie*& result );
  bool alterStock( const string&, int );

private:
	struct Node {
		Movie* data;						// pointer to data object
		Node* left;							// left subtree pointer
		Node* right;						// right subtree pointer
	};

	Node* root;								// root of the tree

  void destroyTree( Node* );
  Node* insertHelper( Node*, Node* );
  void printHelper( Node*, ostream& ) const;
	bool retrieve( const Movie& target, Node*& result );
  bool retrieveHelper( Node*, const Movie&, Movie*& );
  bool retrieveHelper( Node*, const Movie&, Node*& );
};

#endif // BINTREE_H
