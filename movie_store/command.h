// ---------------- command.h -----------------

/*
*  Superclass to represent an action on the 
*  system by an external actor.
*/

#ifndef COMMAND_H
#define COMMAND_H

#include "stock.h"
#include "customers.h"

class Command
{
public:
    Command() {}
    virtual ~Command() {}
    virtual void execute(Stock&, Customers&) = 0; 
};

#endif // COMMAND_H
