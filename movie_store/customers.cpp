// customers.cpp
// -----------------------------------------------------------------------------
// stores the customers for this store in a hash table
// -----------------------------------------------------------------------------


#include "customers.h"


// constructor
// -----------------------------------------------------------------------------
Customers::Customers() {
  // initialize table to NULL 
  for ( int i = 0; i < 29; ++i )
    table[i] = NULL;
}

// destructor
// -----------------------------------------------------------------------------
Customers::~Customers() {
  Customer** itr = table; // create iterator to table 
  // iterate over the table and delete any non-NULL entries
  for ( int i = 0; i < 29; ++i )
  {
    if( itr )
    {
      Customer** temp = itr;
      ++itr;
      delete *temp;
    } else
      ++itr;
  }
}

void Customers::displayAll()
{
for (int i = 0; i < 29; ++i )
  if (table[i])
    cout << table[i]->getID() << endl;
}


// get
// -----------------------------------------------------------------------------
// return a pointer to a customer in this table
// -----------------------------------------------------------------------------
Customer* Customers::get( int id )
{
  int hashNum = id % 29;
  for ( int i = 0; i < 29 && table[hashNum]; ++i ) {
    if ( table[hashNum]->getID() == id )
      return table[hashNum];
    else
      hashNum = i * ( 13 - id % 13 );
  }
  return NULL;
}

// add
// -----------------------------------------------------------------------------
// add a customer to this table
// -----------------------------------------------------------------------------
bool Customers::add( Customer &cust ) {
  int hashNum = cust.getID() % 29;
  for ( int i = 0; i < 29; ++i ) {
    if ( !table[hashNum] ) {
      table[hashNum] = &cust;
      return true;
    } else
      hashNum = i * ( 13 - cust.getID() % 13 );
  }
  return false; 
}

