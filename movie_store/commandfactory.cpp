// commandfactory.cpp
// -----------------------------------------------------------------------------
// Factory design pattern to produce Command objects
// -----------------------------------------------------------------------------

#include "commandfactory.h"

CommandFactory::CommandFactory( )
{ }

CommandFactory::~CommandFactory( )
{ }


Command* CommandFactory::makeCommand( istringstream& input )
{
  char commandType = input.get();

  if ( commandType != 'I' && commandType != 'H' && commandType != 'B'
       && commandType != 'R' )
  {
    cout << "invalid command type" << endl;
    return NULL;
  }

  switch( commandType )
  {
    case 'I':
    {
      return new Inventory( );
      break;
    }
    case 'H':
    {
      int id;
      input >> id;
      return new History( id );
      break;
    }
  }

  // start parsing data for documentable commands
  int id;
  input >> id;
  char format, category;
  input >> format >> category;
  if ( ( category != 'C' && category != 'D' && 
       category != 'F') || format != 'D' )
  {
    cout << "invalid movie format or category" << endl;
    return NULL; 
  }

  // create a dummy search movie
  Movie* dummy;
  if ( category == 'C' )
  {
    int month, year;
    string firstName, lastName;
    input >> month >> year >> firstName >> lastName;
    string actor = firstName + " " + lastName;
    dummy = new ClassicMovie( category, 'D', 1, "", "", year, actor, month );
  }
  else
  {
    char c;
    string first;
    input.get(); // burn a letter
    while ( (c = input.get()) != ',')
      first += c;
    if ( category == 'D' )
    {
      string title;
      input.get(); // burn a letter
      while ( (c = input.get()) != ',')
        title += c;
      dummy = new Movie( category, 'D', 1, first, title, 0, "", 0 );
    }
    else // category == 'F'
    {
      int year;
      input >> year;
      dummy = new Movie( category, 'D', 1, "", first, year, "", 0 );
    }
  }

  switch (commandType)
  {
    case 'B':
      return new Borrow(id, *dummy);
      break;
    case 'R':
      return new Return(id, *dummy);
      break;
    default:
      cout << "invalid command type" << endl;
  }
}
