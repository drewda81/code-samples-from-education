// borrow.cpp
// -----------------------------------------------------------------------------
// When executed, the Borrow object attempts to decrement the count for a
// specified Movie and adds a transaction to a specified Customer's History.
// -----------------------------------------------------------------------------

#include "borrow.h"

// default constructor
// -----------------------------------------------------------------------------
Borrow::Borrow( int aCustomerID, Movie& aDummyMovie )
              : DocumentableCommand( aCustomerID, aDummyMovie ) 
{ }

// destructor
// -----------------------------------------------------------------------------
Borrow::~Borrow()
{ 
  delete dummyMovie;
}

// borrow
// -----------------------------------------------------------------------------
// Borrow an time from this system's Stock. Executed by the Business component.
// If successful, appends the details of the transactino to the specified
// Customer's History. If unsuccessful, error message will be outputted by the
// Stock.
void Borrow::execute( Stock& theStock, Customers& theCustomers )
{
  Customer* theCustomer = theCustomers.get(customerID);
  if ( !theCustomer )
  {
    cout << "customer not found" << endl;
    return;
  }

  Movie* theMovie = theStock.decrement(*dummyMovie);
  if ( theMovie )
  {
    theCustomer->document('B', *theMovie);
  }
  else
  {
    cout << "movie not found" << endl;
  }

}
