//--------------------- return.h -----------------------

/*
 *  Return is a subclass of DocumentableCommand. If Movie
 *  exists in Stock, increment Stock for that Movie. Else
 *  return an error message.
 */
#ifndef RETURN_H_
#define RETURN_H_

#include "documentablecommand.h"
#include <string>
#include <iostream>
#include "stock.h"
#include "customers.h"

class Return : public DocumentableCommand {
public:
  Return(int, Movie&);  // params: customer ID, movie description
  ~Return();
  void execute( Stock&, Customers& );
};

#endif // ndef RETURN_H_

