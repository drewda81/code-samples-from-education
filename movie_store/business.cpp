// business.cpp
// -----------------------------------------------------------------------------
// handles the business for this store
// -----------------------------------------------------------------------------

#include "business.h"

// default constructor
// -----------------------------------------------------------------------------
Business::Business( )
{ } 

// destructor
// -----------------------------------------------------------------------------
Business::~Business()
{ }

// execute
// -----------------------------------------------------------------------------
// execute a command on this business's stock and customers
// -----------------------------------------------------------------------------
void Business::execute( Command &aCommand )
{
  aCommand.execute( theStock, theCustomers );

  delete &aCommand;
}

// addMovie
// -----------------------------------------------------------------------------
// add a movie to this stock 
// -----------------------------------------------------------------------------
void Business::addMovie( Movie& aMovie )
{
  theStock.addStock( aMovie );
}

// addCustomer
// -----------------------------------------------------------------------------
// add a customer to this customers 
// -----------------------------------------------------------------------------
void Business::addCustomer( Customer& cust )
{
  theCustomers.add( cust );
}

// displayCustomers
// -----------------------------------------------------------------------------
// displays the ID of all customers for this store
// -----------------------------------------------------------------------------
void Business::displayCustomers()
{
  theCustomers.displayAll();
}
