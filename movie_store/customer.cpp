// customer.cpp
// ----------------------------------------------------------------------------
// representation of a customer in this system
// ----------------------------------------------------------------------------


#include "customer.h"

// default constructor
// ----------------------------------------------------------------------------
Customer::Customer(int id, string nm) : customerID(id), name(nm) {}

// destructor 
// ----------------------------------------------------------------------------
Customer::~Customer() {}

// displayHistory
// ----------------------------------------------------------------------------
// Output's this Customer's History one line at a time to the console.
// ----------------------------------------------------------------------------
void Customer::displayHistory()
{
  cout << "Customer No. " << customerID << endl;

  for ( int i = 0; i < history.size(); ++i )
    cout << history[i] << endl;

  cout << endl;

}

// document
// ----------------------------------------------------------------------------
// Add a transaction to this Customer's History.
// ----------------------------------------------------------------------------
void Customer::document( char commandType, Movie& aMovie )
{
  string result;
  result.push_back(commandType);
  result += ", ";
  result += aMovie.toString();

  history.push_back( result );
}
