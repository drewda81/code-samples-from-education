// history.cpp
// -----------------------------------------------------------------------------
// A subclass of Command. When executed, History displays all of a customer's
// borrows and returns.
// -----------------------------------------------------------------------------

#include "history.h"

// constructor
// -----------------------------------------------------------------------------
History::History( int aCustomerID ) : customerID( aCustomerID )
{ }

// execute
// -----------------------------------------------------------------------------
// Executes this History command on this system's Customers
// -----------------------------------------------------------------------------
void History::execute( Stock& theStock, Customers& theCustomers )
{
  Customer* theCustomer = theCustomers.get(customerID);
  if (theCustomer)
  {
    theCustomer->displayHistory( );
  }
  else
    cout << "customerID " << customerID << " does not exist." << endl;
}
