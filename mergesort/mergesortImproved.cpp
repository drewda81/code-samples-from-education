// Drew Andersen
// CSS342: Program 4
// This program sorts a vector using an iterative merge sort method

#include<vector>
using namespace std;

// Helper function to merge two segments from vector c into vector d. The left
// tail of the first segment is lt, the midpoint is md and the right tail is rt.
template<class Type>
void merge(vector<Type> &c, vector<Type> &d, int lt, int md, int rt) {
  int i = lt; // position of first element in left segment of vector c
  int j = md+1; // position of first element in right segment of vector c
  int k = lt; // position of first element in merged segment in vector d 

  // merge until i or j exits its segment
  while ((i <= md) && (j <= rt)) {
    if (c[i] <= (c[j]))
      d[k++] = c[i++]; // set new value in d, then increment k and i
    else
      d[k++] = c[j++]; // set new value in d, then increment k and j
  }

  // clean up remaining eleements
  while (i <= md)
    d[k++] = c[i++]; // set new value in d, then increment k and i
  while (j <= rt)
    d[k++] = c[j++]; // set new value in d, then increment k and j
}

// Helper function to perform one pass of merge sort from  array x into array y
// for segment size seg and dataset size n.
template<class Type>
void mergePass(vector<Type> &x, vector<Type> &y, int seg, int n) {
  
  // merge adjacent segments
  int i = 0;
  while (i <= n - 2 * seg) {
    // merge two adjacent segments of size seg
    merge(x, y, i, (i + seg - 1), (i + 2 * seg - 1));
    i = i + 2 * seg;
  }

  //  merge remaining elements when less than 2 segmeents remain on this pass
  if (i + seg < n)
    merge (x, y, i, i + seg - 1, n - 1);
  else
    for (int j = i; j <= n - 1; j++)
      y[j] = x[j];
}

// interface for mergesortImproved. Takes a vector of any type as input.
template<class Comparable>
void mergesortImproved (vector<Comparable> &a) {

  int n = a.size(); // size of dataset to be sorted
  int seg = 1; // the size of segments to be sorted, doubles each pass
  vector<Comparable> b(n); // declare and initialize working vector 

  // make passes on the data until the segment size is larger than or equal to
  // the size of the data set
  while (seg < n) {
    mergePass(a, b, seg, n); // merge from a to b
    seg += seg; // double segment size
    mergePass(b, a, seg, n); // merge from b to a
    seg += seg; // double segment size
  }
}
