#include"longint.h"
#include<string>
using namespace std;

/* Andrew Andersen
   CSS342

   The LongInt class allows a user to store a number of size limited only to 
   available memory.
   Assumptions: the remove0s method runs at the end of any method where the 
   possiblity of a zero occurring, thus there should never be a longint with
   just the number zero. It will always be empty.
*/


int ASCII_CONVERSION = 48; // used to convert from char to int and int to char

/*============= Constructors ==============*/

// Constructor with string 
LongInt::LongInt( const string str ) {

  // create iterator over string
  string::const_iterator it = str.begin();

  // check if the first char is a negative sign
  if (*it == '-') {
    negative = true;
    it++;
  } else negative = false;

  // check if char is a digit from 0 to 9, otherwise skip it
  for ( ; it != str.end(); it++ ) 
    if ( *it >= 0 + ASCII_CONVERSION && *it <= 9 + ASCII_CONVERSION ) 
      digits.addBack( *it );

  // remove any extraneous zeros from beginning of number
  this->remove0s();

}

// Copy Constructor
LongInt::LongInt( const LongInt &rhs ) : negative(rhs.negative) ,
   digits(rhs.digits) { }

// Default Constructor
LongInt::LongInt( ) {
  negative = false;
}

// Destructor
LongInt::~LongInt( ) {

}

/*========== Stream operators ==========*/

// output opterator
ostream& operator<<( ostream &out, const LongInt &rhs ) {

  if ( rhs.digits.isEmpty( ) ) 
    out << 0;
  
  else {
    if ( rhs.negative )
      out << "-";

    LongInt temp = rhs;
    char current;

    for (int i = 0; i < rhs.digits.size(); i++) {
      current = temp.digits.removeFront(); 
      out << current;
    }
  }
  return out;
}

// input operator
istream& operator>>( istream &in, LongInt &rhs ) {
  string temp;
  in >> temp;

  rhs = LongInt( temp );

  return in; 
}

/*========== Arithmetic binary operators ==========*/

// + operator
// add rhs to this and return the result
LongInt LongInt::operator+( const LongInt &rhs ) const {
  
  // case when lhs is positive and rhs is negative
  if ( !this->negative && rhs.negative ) {
    LongInt temp = rhs;
    temp.negative = false;
    return *this - temp;
  }

  // case when lhs is negative and rhs is positive
  if ( this->negative && !rhs.negative ) {
    LongInt temp = *this;
    temp.negative = false;
    return rhs - temp;
  }

  // cases when lhs and rhs have the same sign

  // create copies in order to access elements
  LongInt lhsTemp = *this;
  LongInt rhsTemp = rhs;
  LongInt sum;
  sum.negative = this->negative; // because we covered opp sign cases, so 
                                 // this->neg = rhs->neg

  // store current characters from each digit
  char lhsCurrent;
  char rhsCurrent;

  int carry = 0; // store the overflow for results > 10 

  // add digits while lhs has digits
  for ( int i = 0; i < this->digits.size( ); i++ ) {
    lhsCurrent = lhsTemp.digits.removeBack( ) - ASCII_CONVERSION;

    // ensure rhs still has digits to contribute, otherwise add 0
    if ( !rhsTemp.digits.isEmpty() ) 
      rhsCurrent = rhsTemp.digits.removeBack( ) - ASCII_CONVERSION;
    else rhsCurrent = 0;

    // add the current digits together and calculate the carryover 
    sum.digits.addFront( ( lhsCurrent + rhsCurrent + carry ) % 10 
                              + ASCII_CONVERSION );
    carry = ( lhsCurrent + rhsCurrent + carry ) / 10;
  }

  // while rhs still has digits, add them to the front of the result
  while ( !rhsTemp.digits.isEmpty( ) ) { 
    rhsCurrent = rhsTemp.digits.removeBack( ) - ASCII_CONVERSION; 
    sum.digits.addFront( ( rhsCurrent + carry ) % 10 + ASCII_CONVERSION );
    carry = ( rhsCurrent + carry ) / 10; 
  }

  sum.digits.addFront( carry + ASCII_CONVERSION ); // add the last carry

  sum.remove0s();

  return sum;

}

// - operator
// Subtract rhs from this
LongInt LongInt::operator-( const LongInt &rhs ) const {

  // scenario for or equal long-ints
  if ( *this == rhs || this->digits.isEmpty( ) && rhs.digits.isEmpty( ) ) {
    LongInt temp = LongInt( ); // return an empty LongInt;
    return temp;
  }

  // scenario for -lhs - rhs
  if ( this->negative && !rhs.negative ) {
    LongInt temp = rhs;
    temp.negative = true;
    return ( *this + temp );
  }

  // scenario for lhs - (-rhs)
  if ( !this->negative && rhs.negative ) {
    LongInt temp = rhs;
    temp.negative = false;
    return ( *this + temp ); 
  }

  // scenario for lhs - 0 rhs
  if ( rhs.digits.isEmpty( ) ) 
    return *this;

  // scenario for 0 - rhs;
  if ( this->digits.isEmpty( ) ) {
    LongInt rhsTemp = rhs;
    rhsTemp.negative = !rhsTemp.negative;
    return rhsTemp;
  }
  
  // all other scenarios 
  LongInt minuend, subtrahend, difference = LongInt();
  char minCurrent, subCurrent; 

  if ( *this > rhs && !this->negative ) {
    minuend = *this;
    subtrahend = rhs;
    difference.negative = false;
  } else if ( *this < rhs && !this->negative ) {
    minuend = rhs;
    subtrahend = *this;
    difference.negative = true;
  } else if ( *this > rhs && this->negative ) {
    minuend = rhs;
    subtrahend = *this;
    difference.negative = false;
  } else {
    minuend = *this;
    subtrahend = rhs;
    difference.negative = true;
  }

  // perform the subtraction operation
  int borrow = 0; // used to indicate if a borrow is necessary
  while( !subtrahend.digits.isEmpty( ) ) {
    minCurrent = minuend.digits.removeBack( );
    subCurrent = subtrahend.digits.removeBack( );
 
    if ( minCurrent + borrow >= subCurrent ) { 
      difference.digits.addFront( minCurrent - borrow - subCurrent
        + ASCII_CONVERSION );
      borrow = 0;
    } else {
      difference.digits.addFront( 10 + minCurrent - borrow - subCurrent
        + ASCII_CONVERSION );
      borrow = 1;
    }
  }

  while ( !minuend.digits.isEmpty( ) ) {
    minCurrent = minuend.digits.removeBack( );
    difference.digits.addFront ( minCurrent - borrow );
    borrow = 0;
  }
  
  difference.remove0s( );
  return difference;
}

/*========== Assignment Operator ==========*/
const LongInt & LongInt::operator=( const LongInt &rhs ) {
  this->negative = rhs.negative;
  this->digits = rhs.digits;
}

/*========== Logical Binary Operators ==========*/

// < operator
// Returns true if this is less than rhs, false otherwise
bool LongInt::operator< ( const LongInt & rhs ) const {
  if ( this->digits.isEmpty( ) && rhs.digits.isEmpty( ) )
    return false;
  else if ( !this->negative && rhs.negative )
    return true;
  else if ( this->negative && !rhs.negative )
    return false;
  else if ( this->digits.size( ) < rhs.digits.size( ) ) { 
    if ( this->negative )
      return false;
    else return true;
  } else if ( this->digits.size( ) > rhs.digits.size( ) ) {
    if ( this->negative )
      return true;
    else return false;
  } else { // this->digits.size( ) == rhs.digits.size( ) 
    LongInt lhsTemp = *this;
    LongInt rhsTemp = rhs;
    char lhsCurrent;
    char rhsCurrent;

    // compare each digit of respective LongInts
    while ( !lhsTemp.digits.isEmpty( ) ) {
      lhsCurrent = lhsTemp.digits.removeFront( );
      rhsCurrent = rhsTemp.digits.removeFront( );
      if ( lhsCurrent < rhsCurrent )
        if ( this->negative )
          return false;
        else return true;
      if ( lhsCurrent > rhsCurrent )
        if ( this->negative )
          return true;
        else return false;
    } 
    return false; // because they are equal
  }
}

// <= operator
// returns true if this is less than or equal to rhs, false otherwise
bool LongInt::operator<=( const LongInt & rhs ) const {
  return *this < rhs || *this == rhs;
}

// > operator
// Returns true if this is greater than rhs, false otherwise
bool LongInt::operator> ( const LongInt & rhs ) const {
  return !( *this <= rhs );
}

// >= operator
// returns true if this is greater than or equal to rhs, false otherwise
bool LongInt::operator>=( const LongInt & rhs ) const {
  return !( *this < rhs );
}

// == operator
// Returns true if this is equal to rhs, false otherwise
bool LongInt::operator==( const LongInt & rhs ) const {
  if ( this->digits.size( ) == 0 && rhs.digits.size( ) == 0 )
    return true;
  else if ( this->digits.size( ) != rhs.digits.size( ) )
    return false;
  else if ( this->negative != rhs.negative )
    return false;
  else { // this->digits.size( ) == rhs.digits.size( ) 
    LongInt lhsTemp = *this;
    LongInt rhsTemp = rhs;
    char lhsCurrent;
    char rhsCurrent;

    // compare each digit of respective LongInts
    while ( !lhsTemp.digits.isEmpty( ) ) {
      lhsCurrent = lhsTemp.digits.removeFront( );
      rhsCurrent = rhsTemp.digits.removeFront( );
      if ( lhsCurrent != rhsCurrent )
        return false;
    } 
    return true; // because they are equal   
  }   
}

// != operator
// Returns false if this is equal to rhs, true otherwise
bool LongInt::operator!=( const LongInt & rhs ) const {
  return !(*this == rhs);
}

/*========== Private Methods ==========*/

// remove0s
// Removes superflous 0 digits from beginning of LongInt
void LongInt::remove0s( ) {

  // ensure 0 is always positive for comparing 0 to 0; 
  if ( digits.isEmpty() ) {
    negative = false;
    return;
  }

  char tmp = digits.removeFront( );
  while (tmp == '0' && !digits.isEmpty( ) )
    tmp = digits.removeFront( ); 

  digits.addFront( tmp );
  
  if ( digits.isEmpty() )
    negative = false;
}
