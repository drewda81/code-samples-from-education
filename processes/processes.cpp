/*
  Drew Andersen
  CSS430
  Program 1, Part 1
  2016-04-13
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
using namespace std;

void handle_fork_error()
{
  cerr << "Fork Error" << endl;
  exit(-1);
}

int main(int argc, char *argv[])
{

  if (argc != 2)
  {
    cerr << "usage: processes PHRASE" << endl;
    exit(-1);
  }

  // create child
  pid_t pid = fork();

  if (pid < 0) handle_fork_error();

  else if (pid == 0) // is child
  {
    enum{READ, WRITE};

    // create pipe to connect child to grandchild
    int pipe1[2];
    pipe(pipe1);
    pid = fork();

    if (pid < 0) handle_fork_error();

    else if (pid == 0) // is grandchild
    {
      // create pipe to connect grandchild to great-grandchild
      int pipe2[2];
      pipe(pipe2);
      pid = fork();

      if (pid < 0) handle_fork_error();

      else if (pid == 0) // is great grandchild
      {
        close(pipe1[READ]);
        dup2(pipe2[WRITE], 1);
        execlp("ps", "ps", "-A", NULL);
        exit(0);
      }
      else // return to grandchild
      {
        close(pipe1[READ]);
        dup2(pipe1[WRITE], 1);
        close(pipe2[WRITE]);
        dup2(pipe2[READ], 0);
        wait(NULL);
        execlp("grep", "grep", argv[1], NULL);
        exit(0);
      }
    }
    else // return to child
    {
      close(pipe1[WRITE]);
      dup2(pipe1[READ], 0);
      wait(NULL);
      execlp("wc", "wc", "-l", NULL);
      exit(0);
    }
  }
  else // return to parent
  {
    wait(NULL);
    
  }
  return 0;
} 
