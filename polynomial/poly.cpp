// ---------------------------------- Poly.h ----------------------------------
// Andrew Andersen CSS343A
// Date: 2016-01-16
// Last Modified: 2016-01-14
// -----------------------------------------------------------------------------
// A class to create a manipulate polynomials
// -----------------------------------------------------------------------------
// Notes
// * Assumes user will never enter a negative exponent of x. 
// * Assumes user will only enter pairs of integers for overloaded >> operator
// -----------------------------------------------------------------------------

#include"poly.h"

// --------------------------- Default Construtor ------------------------------ 
// Description: Creates an object of type Poly
// - int coefficient: the coefficient of the largest power of x, default is 0
// - int degree: the largest power of x, default is 0
// Only handles positive exponents of x. If negative degree parameter is passed,
// each of coefficient and degree are set to 0. 
// -----------------------------------------------------------------------------
Poly::Poly( int coefficient, int degree )
{
  if ( degree < 0 )
  {
    coefficient = 0;
    degree = 0;
  }

  size = degree + 1;

  coefficients = new int[ size ];
  coefficients[ degree ] = coefficient;

  // initialize all values in coefficients
  for (int i = 0; i < size - 1; i++ )
    coefficients[i] = 0;

}

// ---------------------------- Copy Constructor ------------------------------- 
// Description: Creates an object of type Poly
// - const Poly &original: the Poly to copy to create this Poly
// -----------------------------------------------------------------------------
Poly::Poly( const Poly &original )
{
  *this = original;
}

// ------------------------------- Destructor ---------------------------------- 
// Description: destroys a Poly
// -----------------------------------------------------------------------------
Poly::~Poly( )
{
  delete coefficients;
}

// ------------------------------- = Operator ---------------------------------- 
// Description: copies data from a Poly to this Poly
// - const Poly& rhs: the Poly from which to copy data 
// -----------------------------------------------------------------------------
const Poly& Poly::operator = ( const Poly& rhs )
{

  if ( rhs == *this )
    return *this;

  coefficients = new int[rhs.size];
  for ( int i = 0; i < rhs.size; i++ )
    coefficients[i] = rhs.coefficients[i];
  size = rhs.size;

  return *this;
}


// ------------------------------- + Operator ---------------------------------- 
// Description: adds rhs to this and returns the result 
// -----------------------------------------------------------------------------
Poly Poly::operator + ( const Poly & rhs ) const
{

  Poly result ( ( size >= rhs.size ) ? *this : rhs );

  if ( size >= rhs.size )
  {
    for ( int i = 0; i < rhs.size; i++ )
      result.coefficients[i] = coefficients[i] + rhs.coefficients[i];
    for ( int i = rhs.size; i < size; i++ )
      result.coefficients[i] = coefficients[i];
  }
  else
  {
    for ( int i = 0; i < size; i++ )
      result.coefficients[i] = coefficients[i] + rhs.coefficients[i];
    for ( int i = size; i < rhs.size; i++ )
      result.coefficients[i] = rhs.coefficients[i];
  }

  return result;
}

// ------------------------------- - Operator ---------------------------------- 
// Description: subtracts rhs from this poly
// -----------------------------------------------------------------------------
Poly Poly::operator - ( const Poly & rhs ) const
{

  Poly result ( ( size >= rhs.size ) ? *this : rhs );

  if ( size >= rhs.size )
  {
    for ( int i = 0; i < rhs.size; i++ )
      result.coefficients[i] = coefficients[i] - rhs.coefficients[i];
    for ( int i = rhs.size; i < size; i++ )
      result.coefficients[i] = coefficients[i];
  }
  else
  {
    for ( int i = 0; i < size; i++ )
      result.coefficients[i] = coefficients[i] - rhs.coefficients[i];
    for ( int i = size; i < result.size; i++ )
      result.coefficients[i] = -( rhs.coefficients[i] );
  }

  return result;
}

// ------------------------------- * Operator ---------------------------------- 
// Description: multiplies this poly by rhs
// -----------------------------------------------------------------------------
Poly Poly::operator * ( const Poly & rhs ) const
{
  Poly result ( 0, size + rhs.size );

  for ( int i = 0; i < size; i++ )
  {
    for ( int j = 0; j < rhs.size; j++ )
    {
      int coefficient = coefficients[i] * rhs.coefficients[j];
      int degree = i + j;
      result.setCoeff( coefficients[i] * rhs.coefficients[j] + 
                       result.coefficients[ i + j ], i + j );
    }
  }
  return result;
}

// ------------------------------- += Operator --------------------------------- 
// Description: adds and assigns rhs to this poly 
// -----------------------------------------------------------------------------
Poly Poly::operator += ( const Poly & rhs ) const
{
  Poly result = *this + rhs;
  return result;
}

// ------------------------------- -= Operator --------------------------------- 
// Description: subtracts and assigns rhs to this poly 
// -----------------------------------------------------------------------------
Poly Poly::operator -= ( const Poly & rhs ) const
{
  Poly result = *this - rhs;
  return result;
}

// ------------------------------- *= Operator --------------------------------- 
// Description: multiplies and assigns rhs to this poly
// -----------------------------------------------------------------------------
Poly Poly::operator *= ( const Poly & rhs ) const
{
  Poly result = *this * rhs;
  return result;
}

// ------------------------------- == Operator --------------------------------- 
// Description: Returns true if this is equal to rhs, false otherwise
// -----------------------------------------------------------------------------
bool Poly::operator == ( const Poly& rhs ) const
{
  if ( size != rhs.size )
    return false;

  for ( int i = 0; i < size; i++)
    if ( coefficients[i] != rhs.coefficients[i] )
      return false;

  return true;
}

// ------------------------------- != Operator --------------------------------- 
// Description: Returns true if this is not equal to rhs, false otherwise 
// -----------------------------------------------------------------------------
bool Poly::operator != ( const Poly& rhs ) const
{
  return !( *this == rhs );
}

// ------------------------------- << Operator --------------------------------- 
// Description: Outputs this polynomial to an ostream 
// -----------------------------------------------------------------------------
ostream& operator << ( ostream &output, const Poly& input )
{
  // when size = 0 the output will of course be zero
  if ( input.size == 0 )
  {
    output << 0;
    return output;
  }

  // flag to eliminate space and plus-sign for highest degree
  bool first = true;

  // Starting from the tail of the array, decrement and print each coefficient
  for ( int i = input.size - 1; i >= 0; i-- )
  {
    if ( input.coefficients[i] != 0 )
    {
      if ( !first )
        {
        output << " ";
        if ( input.coefficients[i] > 0 )
          output << "+";
        }
      if ( i > 0 )
      {
        if ( input.coefficients[i] != 1 )
          output << input.coefficients[i];
        output << "x";
        if ( i > 1 )
          output << "^" << i;
      }
      else
        output << input.coefficients[i];
    first = false;
    }
    if ( first == true && i == 0 && input.coefficients[i] == 0 )
      output << 0;
  }
  return output;
}

// ------------------------------- >> Operator ---------------------------------
// Description: Accepts pairs of integers and assigns them to a given poly as 
// coefficient and degree, respectively. 
// -----------------------------------------------------------------------------
istream& operator >> ( istream &input, Poly& poly )
{
  int coeff = 0;
  int degree = 0;

  input >> coeff >> degree;

  while ( coeff != -1 && degree != -1 )
  {
    poly.setCoeff( coeff, degree );
    input >> coeff >> degree;
  }

  return input;
}


--------------

void Poly::integrate()
{
  double* crnt = coefficients;
  double* next = coefficients;
  for( int i = 1; i < size; i++ )
  {
    *current = *next * (i+1);
    current++;
    *next = 0;
    next++;
  }
}
  

// -------------------------------- getCoeff ----------------------------------- 
// Description: returns the coefficient for given degree of x 
// -----------------------------------------------------------------------------
int Poly::getCoeff ( int degree )
{
  if ( degree < size && degree >= 0 )
    return coefficients[degree];
  else
    return 0; 
}

// -------------------------------- setCoeff ----------------------------------- 
// Description: Sets the coefficient for a given degree of this polynomial
// If the provided degree is less than zero, the degree of zero is used.
// -----------------------------------------------------------------------------
void Poly::setCoeff ( int coefficient, int degree )
{
  // set degree to zero if less than zero entered for coefficient
  if ( degree < 0 )
  {
    coefficients[0] = coefficient;
  }
  // when this poly's coefficients array is larger than the degree parameter
  else if ( degree < size && degree >= 0 )
  {  
    coefficients[ degree ] = coefficient;
  }
  // for all other cases, create a new array to store the coefficients
  else 
  {
    int* newCoeffs = new int[ degree + 1 ];
    // fill the new array with the old array
    for ( int i = 0; i < size; i++ )
      newCoeffs[i] = coefficients[i];
    // initialize the rest of the new array to the end with zeros
    for ( int i = size; i < degree; i++ )
      newCoeffs[i] = 0;
    // set the max value in the array to the new coeffficient
    newCoeffs[ degree ] = coefficient;

    size = degree + 1;
    int* oldCoeffs = coefficients;
    coefficients = newCoeffs;
    delete[] oldCoeffs;
  }
}
