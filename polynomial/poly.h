// ---------------------------------- Poly.h ----------------------------------
// Andrew Andersen CSS343A
// Date: 2016-01-16
// Last Modified: 2016-01-14
// -----------------------------------------------------------------------------
// A class to create a manipulate polynomials
// -----------------------------------------------------------------------------
// Notes
// * Assumes user will never enter a negative exponent of x. 
// * Assumes user will only enter pairs of integers for overloaded >> operator
// -----------------------------------------------------------------------------

#ifndef POLY_H
#define POLY_H
#include <iostream>
using namespace std;

class Poly {

friend ostream& operator << ( ostream&, const Poly& );
friend istream& operator >> ( istream&, Poly& );

public:

  Poly( int = 0, int = 0 );
  Poly( const Poly & );
  ~Poly( );

  const Poly& operator = ( const Poly & );

  Poly operator + ( const Poly & ) const;
  Poly operator - ( const Poly & ) const;
  Poly operator * ( const Poly & ) const;

  Poly operator += ( const Poly & ) const;
  Poly operator -= ( const Poly & ) const;
  Poly operator *= ( const Poly & ) const;

  bool operator == ( const Poly & ) const;
  bool operator != ( const Poly & ) const;

  int getCoeff ( int );
  void setCoeff ( int, int );
  void integrate();

private:

  int* coefficients;
  int size;

};

#endif //POLY_H
