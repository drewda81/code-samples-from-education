// a complex object holds one real number, one imaginary number
// Drew Andersen

#include "complex.h"

//----------------------------------------------------------------------------
// complex 
// Parameters are real and imgainary components, respectively.
// Takes zero, one, two parameters. Real and imaginary components have default
// values of zero. 
 
complex::complex(double real, double img) {
  this->real = real;
  this->img = img;
  this->divZero = false;
}

//------------------------------------------------------------------------------
// operator+  
// overloaded +: addition of 2 complex numbers, current object and parameter

complex complex::operator+(const complex& a) const {
  complex sum;

  sum.real = a.real + real;
  sum.img = a.img + img;

  return sum;
}

//------------------------------------------------------------------------------
// operator-  
// overloaded -: subtraction of 2 complex numbers, current object and parameter

complex complex::operator-(const complex& s) const {
  complex sub;

  sub.real = real - s.real;
  sub.img = img - s.img;

  return sub;
}

//------------------------------------------------------------------------------
// operator*  
// overloaded *: multiplication of 2 complex numbers, current object and
// parameter

complex complex::operator*(const complex& m) const {
  complex mult;

  mult.real = real * m.real - img * m.img; // i^2 = -1
  mult.img = img * m.real + m.img * real;

  return mult;
}
 
//------------------------------------------------------------------------------
// operator/  
// overloaded /: division of 2 complex numbers, current object and parameter

complex complex::operator/(const complex& v) const {
  complex div;

  // if either the complex or real element of the demoninator is zero, trigger 
  // the divZero flag
  if (v.real == 0 && v.img == 0) {
    div.real = 0;
    div.img = 0;
    div.divZero = true;
  }
  else {
    // see http://mathworld.wolfram.com/complexDivision.html for details
    div.real = (real * v.real + img * v.img) / (v.real * v.real + v.img * v.img);
    div.img = (img * v.real - real * v.img) / (v.real * v.real + v.img * v.img);
  }

  return div;
}

//------------------------------------------------------------------------------
// operator== 
// overloaded ==: equality of 2 complex  numbers, current object and parameter

bool complex::operator==(const complex& r) const {
  return (real == r.real && img == r.img);
}

//------------------------------------------------------------------------------
// operator!= 
// overloaded !=: inequality of 2 complex  numbers, current object and parameter

bool complex::operator!=(const complex& r) const {
  return !(*this == r);
}

//------------------------------------------------------------------------------
// operator+= 
// overloaded +=: current object = current object + parameter

complex& complex::operator+=(const complex& a) {

  real = real + a.real;
  img = img + a.img;

  return *this;
}

//------------------------------------------------------------------------------
// operator-= 
// overloaded -=: current object = current object - parameter

complex& complex::operator-=(const complex& s) {

  real = real - s.real;
  img = img - s.img;

  return *this;
}

//------------------------------------------------------------------------------
// operator*= 
// overloaded *=: current object = current object * parameter
 
complex& complex::operator*=(const complex& m) {

  *this = *this * m;
  return *this;
}

//------------------------------------------------------------------------------
// operator/= 
// overloaded /=: current object = current object / parameter
 
complex& complex::operator/=(const complex& v) {

  *this = *this / v;

  return *this;
}

//------------------------------------------------------------------------------
// operator<< 
// overloaded <<: outputs real number, imaginary number i

ostream& operator<<(ostream &output, const complex& r) {
  if (r.divZero == true) 
    output << "Divide by zero error!";
  else if(r.real == 0 && r.img == 0)
    output << 0;
  else if (r.real == 0)
    output << r.img << "i";
  else if (r.img == 0)
    output << r.real;
  else if (r.img < 0)
    output << r.real << r.img << "i";
  else
    output << r.real << "+" << r.img << "i";
}

//------------------------------------------------------------------------------
// operator>> 
// overloaded >>: takes 2 doubles as real and imaginary, does no error checking,
//   standard C casting between floatc, char, etc occurs 

istream& operator>>(istream &input, complex& r) {
  input >> r.real >> r.img;

  return input;
}

//------------------------------------------------------------------------------
// conjugate
// return a new Complex object as the conjugate of the input Complex object

complex complex::conjugate() {
  
  complex conjugate(real, -img);
//  complex conjugate(this->real, -this->img);

  return conjugate;
}
