// a complex object holds one real number, one imaginary
// Drew Andersen

#ifndef COMPLEX_H
#define COMPLEX_H
#include <iostream>
using namespace std;

//---------------------------------------------------------------------------
// Complex Numbers: the field of numbers of the form x + yi where x and y are
// real numbers and i is the imaginary unit equal to the square root of -1.
// Examples include 4 + 6i, 4, and 3i.
//
// Implementation and assumptions:
//   -- complex numbers are stored as real and imginary components
//   -- all arithmetic operators return complex numbers 
//   -- Dividing a complex by zero will cause it to output a divide by zero
//   error when appended to an ostream
//   -- creating a complex number through istream does not include error
//   checking; behavior of improper input is undefined

class complex {

// include friends to allow for istream and ostream operators
friend ostream& operator<<(ostream&, const complex&);
friend istream& operator>>(istream&, complex&);

public:
  // default constructor: paramaeters are real and imaginary resp.
  complex(double = 0, double = 0);

  // arithmetic operators
  complex operator+(const complex &) const;   // add 2 complexs
  complex operator-(const complex &) const;   // subtract 2 complexs
  complex operator*(const complex &) const;   // multiply 2 complexs
  complex operator/(const complex &) const;   // divide 2 complexs
                                              // division by zero terminates 
  // boolean comparison operators
  bool operator==(const complex &) const;     // is object == parameter?
  bool operator!=(const complex &) const;     // is object != parameter?

  // assignment operators
  complex& operator+=(const complex &);       // current object += parameter
  complex& operator-=(const complex &);       // current object -= parameter
  complex& operator*=(const complex &);       // current object *= parameter
  complex& operator/=(const complex &);       // current object /= parameter

  // getter functions (include implementation)
  double getReal () { return real; };         // get the real number
  double getImaginary() { return img; };      // get the imaginary number

  // actions
  complex conjugate ();                       // return conjugate of input

private:
  // data
  double real;                                // real number
  double img;                                 // imaginary number
  bool divZero;                               // been divided by zero 

};

#endif
